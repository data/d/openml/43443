# OpenML dataset: Filipino-Family-Income-and-Expenditure

https://www.openml.org/d/43443

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The Philippine Statistics Authority (PSA) spearheads the conduct of the Family Income and Expenditure Survey (FIES) nationwide. The survey, which is undertaken every three (3) years, is aimed at providing data on family income and expenditure, including, among others, levels of consumption by item of expenditure, sources of income in cash, and related information affecting income and expenditure levels and patterns in the Philippines.
Content
Inside this data set is some selected variables from the latest Family Income and Expenditure Survey (FIES) in the Philippines. It contains more than 40k observations and 60 variables which is primarily comprised of the household income and expenditures of that specific household
Acknowledgements
The Philippine Statistics Authority for providing the publisher with their raw data
Inspiration
Socio-economic classification models in the Philippines has been very problematic. In fact, not one SEC model has been widely accepted. Government bodies uses their own SEC models and private research entities uses their own. We all know that household income is the greatest indicator of one's socio-economic classification that's why the publisher would like to find out the following:
1) Best model in predicting household income
2) Key drivers of household income, we want to make the model as sparse as possible
3) Some exploratory analysis in the data would also be useful

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43443) of an [OpenML dataset](https://www.openml.org/d/43443). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43443/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43443/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43443/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

